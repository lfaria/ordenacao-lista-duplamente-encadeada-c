#include<stdio.h>
#include<stdlib.h>
typedef struct aluno{
     int matricula, idade;
     char nome[51], curso[31], sexo;
}Aluno;
typedef struct no{
     Aluno dados;
     struct no *ant, *prox;
}No;
typedef struct controle{
     No *meio;
     int controleOmeio;
}PontoBinario;

typedef struct lista{
     int tamanho;
     No *primeiro;
}Lista;
void imprime_lista(Lista *lista);
//Função para iniciar a lista, seu tamanho é 0
void inicia_lista(Lista **lista){
     (*lista)->tamanho = 0;
     (*lista)->primeiro = NULL;
}
//função para alocar um nó da lista na memoria
No *aloca_no(){
     No *novo = (No*) calloc(1,sizeof(No));
     if(novo){
          novo->prox = NULL;
          novo->ant = NULL;
          return novo; // retorna o nó se for sucesso
     }
     else
          return NULL; // retorna nulo se for fracasso
}
//função para inserir um no na lista
void insere_lista(Lista *lista, No *novo){
     if((lista)->primeiro == NULL)//lista vazia
          (lista)->primeiro = novo;
     else{
          novo->prox = (lista)->primeiro;
          (lista)->primeiro = novo;
          novo->prox->ant = novo;
     }
     (lista)->tamanho++;
}
//funções auxiliares
void imprime_lista(Lista *lista){
     No *aux = lista->primeiro;
     while(aux){
          printf("%d \t%d \t%c \t%s \t%s\n", aux->dados.matricula, aux->dados.idade, aux->dados.sexo, aux->dados.curso, aux->dados.nome);
          aux = aux->prox;
     }
     printf("%d\n", lista->tamanho);
}
void troca_no(No *menor, No *maior){
     if(menor->ant == maior){// Os nós estão um na frente do outro, o menor é o prox do maior
          menor->ant = maior->ant;
          maior->prox = menor->prox;
          menor->prox = maior;
          maior->ant = menor;
     }
     else{ // Os nós estão em qualquer posição
          No *aux = menor->prox;
          menor->prox = maior->prox;
          maior->prox = aux;

          aux = menor->ant;
          menor->ant = maior->ant;
          maior->ant = aux;
          // no caso de um estar na frente do outro esse passo não é necessario...
          if(menor->prox)
               menor->prox->ant = menor;
          if(maior->ant)
               maior->ant->prox = maior;
     }
     // esse é necessario para os qualquer posição, pois o anterior ao menor e o proximo ao maior nao sabem que sao seus prox e ants, mesmo quando sao nos consecutivos
     if(menor->ant)
          menor->ant->prox = menor;
     if(maior->prox)
          maior->prox->ant = maior;
}
void insere_no(No *posicao, No *inserido){
     if(inserido->prox)
          inserido->prox->ant = inserido->ant;
     if(inserido->ant)
          inserido->ant->prox = inserido->prox;
     inserido->ant = posicao->ant;
     inserido->prox = posicao;
     if(inserido->prox)
          inserido->prox->ant = inserido;
     if(inserido->ant)
          inserido->ant->prox = inserido;
}

//função para ler os dados do arquivo
void ler_dados(Lista *lista){
     char local[81];
     FILE *arquivo;
     printf("Digite o local do arquivo que deseja abrir\n");
     scanf("%s",local);
     if(!(arquivo = fopen(local,"rt")))// se a atribuição falhar retorna NULL, !(não) NULL é verdade, se entrar é pq falhou
          printf("Não foi possivel abrir o arquivo no local %s", local);
     else{
          No *novo = aloca_no(); // cria um nó vazio..
          fscanf(arquivo,"%d %d %c %s %[^\n]", &novo->dados.matricula, &novo->dados.idade, &novo->dados.sexo, novo->dados.curso, novo->dados.nome); // guarda os dados do arquivo em novo..
          while(novo->dados.matricula > 0){
               insere_lista(lista, novo);
               novo = aloca_no();
               fscanf(arquivo,"%d %d %c %s %[^\n]", &novo->dados.matricula, &novo->dados.idade, &novo->dados.sexo, novo->dados.curso, novo->dados.nome);
          }
     }
}
//Metodos de ordenação
void insertion_sort(Lista *lista){
     if(lista->primeiro){
          No *ptInsercao = lista->primeiro, *atual, *novoAtual;
          atual = ptInsercao->prox;
          while(atual){
               novoAtual = atual->prox; // sempre será o proximo em um metodo de insersão
               while(ptInsercao && atual->dados.matricula < ptInsercao->dados.matricula ){// necessário testar se o ptInsercao é nulo.. compara os valores
                    insere_no(ptInsercao, atual);
                    if(atual->ant == NULL) // se for o primeiro
                         lista->primeiro = atual;
                    ptInsercao = atual->ant;// ele sempre é i-1, no caso, atual->ant;
               }
               atual = novoAtual;
               if(atual)
                    ptInsercao = atual->ant; // sempre i-1, atual->ant.. só que aqui eu conserto o local pra usar o ponto certo na proxima comparação
          }
     }
}
PontoBinario busca_binaria(No *atual, No *mid, int inicio, int fim, int meio){
     if(inicio == fim){
          PontoBinario ponto;
          ponto.meio = mid;
          ponto.controleOmeio = fim;
          return ponto;
     }
     if(atual->dados.matricula > mid->dados.matricula){
          for(int i = meio; i < (meio+1+fim)/2; i++)// o novo meio do novo sub vetor por ser calculado (meio+fim+1)/2. Logo, do meio atual até de ele é só eu andar x vezes para direita.
            if(mid->prox)
               mid = mid->prox;
          return busca_binaria(atual, mid, meio+1, fim, (meio+1+fim)/2); // A nova chamada recebe o novo meio, seu inicio é o meio+1 pois o atual é maior que o meio antigo.
     }
     if(atual->dados.matricula < mid->dados.matricula){
          for(int i = meio; i > (inicio+meio)/2; i--)// o meio do novo sub vetor pode ser calculado  por (inicio+meio)/2. Logo, do meio atual até o novo é só andar X vezes para  a esquerda, pecebam que o meio antigo é maior que o novo meio, por isso i-- e o sinal de >
               if(mid->ant)
               mid = mid->ant;
          return busca_binaria(atual, mid, inicio, meio, (meio+inicio)/2);//A nova chamada recebe o novo meio, seu inicio é o mesmo mas seu fim é o meio.
     }
}
void binary_sort(Lista *lista){
     if(lista->primeiro){
          No *atual = lista->primeiro->prox, *novoAtual, *mid = lista->primeiro;
          PontoBinario ptInsercao;
          int conteOmeio = 1, meioAtual = 0; // conteOmeio represeta o fim do atual sub vetor, vai de 1(segundo elemento), até o tamanho do vetor(atual == null)
                                             // meioAtual representa o meio antes de ordenar
          while(atual){
               novoAtual = atual->prox;// essa variavel itera pela lista, não importa oq aconteça, quando o laço terminar o atual sempre será o proximo do atual antes de ser inserido
               if(meioAtual != conteOmeio/2) // conteOmeio aumenta em 1 em cada execução do laço, se a metade dela for maior que o meioAtual significa  que o meio andou
                    mid = mid->prox;
                ptInsercao = busca_binaria(atual, mid, 0, conteOmeio, conteOmeio/2); // conteOmeio é o fim, counteOmeio/2 é meio atual..
               if(ptInsercao.meio && ptInsercao.meio != atual){
                     if(ptInsercao.meio == mid) // se eu for inserir antes do meio dessa função, ele é empurrado pra frente indiceatual+1
                         mid = atual;          // ou seja, o meio agora é o que esta em seu lugar, o atual..
                    else if (ptInsercao.controleOmeio < conteOmeio/2)// Se eu inserir antes do meio atual ele é empurrado pra frente, indiceAtual+1, ou seja, o meio agora é antes dele, porem ele não foi inserido em sua posição, o que permite só andar pra tras.
                        mid = mid->ant;
                    if(ptInsercao.meio->ant == NULL)// caso seja o primeiro da lista
                        lista->primeiro = atual;
                    insere_no(ptInsercao.meio, atual);      
               }
               atual = novoAtual;// itera pelo laço
               meioAtual = conteOmeio / 2; // representa o meio antes de mudar o valor do fim, serve para saber se o meio deve andar ou não
                                          // por exemplo, na primeira iteração temo conteOmeio = 1 e meio atual = 0, no final dela o meioAtual continua zero e o conte o meio = 2
                                         // o meio de um vetor que vai de 0..2 é 1, ou seja, ele aumentou em um, se aumentou um itero o meio lá em cima(mid = mid->prox)
                                                                                      
               conteOmeio++;// aumenta o fim do vetor e me permite saber onde estou
          }
     }
}
void shell_sort(Lista *lista){
     No *atual, *ptInsercao , *novoAtual, *novoPt;
     int gap, i;
     for(gap = 1; gap < lista->tamanho; gap = gap*3+1);
     while(gap > 1){
          gap = gap/3;
          ptInsercao = lista->primeiro;
          atual = ptInsercao;
          for(i = 0; i< gap; i++)
               atual = atual->prox;
          while(atual){
               if(ptInsercao->prox != atual) // quando o gap é 1 e vc troca, o pt de inserção não muda, ex: atual = 2 ptInsercao = 1 (troca), atual = 1, ptInserção = 2;
                    novoPt = ptInsercao->prox;
               else
                    novoPt = ptInsercao;
               novoAtual = atual->prox;
               while(atual->dados.matricula < ptInsercao->dados.matricula){
                    if(ptInsercao->ant == NULL)// primeiro da lista
                         lista->primeiro = atual;
                     troca_no(atual, ptInsercao);
                     if((i - (2*gap)) >= 0){ // i - gap = ptInsercao, i - 2*gap me diz se o novo lugar do atual tem algum elemento gap distancia dele para ser comparado.
                         ptInsercao = atual;
                         for(int j = 0; j < gap; j++)
                              ptInsercao = ptInsercao->ant;      // e tiver eu vou dele até o novo ponto de inserção
                     }
                     else
                       break;   // se não o laço para, pois não há mais trocas entre gap distancias.
               }
               i++; // me diz a posição do atual
               atual = novoAtual;
               ptInsercao = novoPt;
          }
     }
}
void bubble_sort(Lista *lista){
     No *atual ,*fim = NULL;
     int troca = 1;
     while(troca){// só continua se houver troca
          troca = 0;
          atual = lista->primeiro; // sempre deixando o maior no fim, partindo do começo
          while(atual != fim){ // o maior sempre é o ultimo, então não preciso trocar com ele.
               if(atual->dados.matricula > atual->prox->dados.matricula){
                    troca = 1;
                    if(atual->ant == NULL) // se for o primeiro
                         lista->primeiro = atual->prox;
                    troca_no(atual->prox,atual); // se houver troca o atual vai pra frente, ou seja ele é o proximo para a proxima comparação, isso garante o maior no final.
               }
               else
                    atual = atual->prox; // se não ocorrer troca itera com atual->prox;
              if(atual->prox == NULL) // quando chegar no final da lista eu sei qual é o ultimo
                    fim = atual;  // na primeira vez que o laço rodar isso acotence, e quando acontece ele para.
          }
          fim = fim->ant; // como o maior esta no fim, o novo laco vai até fim -1
    }     
}
void shake_sort(Lista *lista){
     No *atual, *comeco = lista->primeiro, *fim = NULL;
     int troca = 1;
     while(troca){
        troca = 0;
        atual = comeco;
        while(atual != fim){
            if(atual->dados.matricula > atual->prox->dados.matricula){
                troca = 1;
                if(atual == comeco)
                    comeco = atual->prox;
                if(atual->ant == NULL)
                    lista->primeiro = atual->prox;
                troca_no(atual->prox, atual);
            }
            else
                atual = atual->prox;
            if(atual->prox == NULL)
                fim = atual;
        }
        fim = fim->ant; // ultimo já é o maior então nao preciso dele
        troca = 0; // sempre zerar pois se ja não houver troca voltando está ordenado
        atual = fim; // esse laço começa do fim
        while(atual != comeco){
            if(atual->ant->dados.matricula > atual->dados.matricula){ // se trocar o atual ja fica no lugar do atual->ant e itera sozinho
                troca = 1;
                if(atual->ant == comeco) // se for o começo
                    comeco = atual;
                if(atual->ant) // se for o primeiro da lista
                    if(atual->ant->ant == NULL)
                        lista->primeiro = atual;
                troca_no(atual, atual->ant);
            }
            else
                atual = atual->ant; // caso não troque é necessario trazer o atual para traz
        }
        comeco = comeco->prox; // o começo é sempre o menor então nao preciso começar nele
        
     }
}
void pente_sort(Lista *lista){
    float fatorShrink = 1.247330950103979;
    int gap = lista->tamanho, troca = 1;
    No *atual, *ptInsercao, *aux;
    while(gap > 1 || troca){
        troca = 0;
        gap = gap/ fatorShrink;
        ptInsercao = lista->primeiro;
        atual = ptInsercao;
        for(int i = 0; i < gap; i++)
            atual = atual->prox;
        while(atual){
            if(atual->dados.matricula < ptInsercao->dados.matricula){
                troca = 1;
                if(ptInsercao->ant == NULL)
                    lista->primeiro = atual;
                aux = atual;
                troca_no(atual, ptInsercao);
                atual = ptInsercao;
                ptInsercao = aux;
            }
            atual = atual->prox;
            ptInsercao = ptInsercao->prox;
        }
    }
}
void quick_sort(Lista *lista){
    No *pivo, *ptInsercao;
}
void main(){
     Lista *listaDeAlunos;
     inicia_lista(&listaDeAlunos);
     ler_dados(listaDeAlunos);
     imprime_lista(listaDeAlunos);
     shake_sort(listaDeAlunos);
     imprime_lista(listaDeAlunos);
}
